<?php $this->load->view('templates/header');?>
<body>
	<div class="wrapper sidebar_minimize">
		<?php $this->load->view('templates/nav');?>		
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">					
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Messages</div>
								</div>
								<div class="card-body">
									<div class="row">
									<div class="col-md-2 col-lg-2"></div>									
										<div class="col-md-8 col-lg-8">
											<?php
												if(!empty($error))
												{
													echo "<div style='color:red; text-align:center;'>";
													if(is_array($error)){
														foreach($err as $error)
														echo $error;
													}
													else
														echo $error;

													echo "</div>";
												}
												else if(!empty($success)){
													echo "<div style='color:green; text-align:center; font-size:20px;'>".$success."</div>";
												}
											?>											
										</div>
									</div>									
								</div>								
							</div>
						</div>
					</div>					
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">						
					</nav>
					<div class="copyright ml-auto">
						<?php echo date('Y'); ?>, made with <i class="fa fa-heart heart text-danger"></i> by <a href="#">ThemeKita</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
	<?php $this->load->view('templates/footer');?>
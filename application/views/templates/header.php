<!DOCTYPE html>

<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/forms/forms.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 04 Oct 2019 20:31:23 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo TITLE; ?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="http://demo.themekita.com/atlantis/livepreview/examples/assets/img/icon.ico" type="image/x-icon"/>
	
	<!-- Fonts and icons -->
	<script src="<?php echo HTTP_JS_PATH; ?>plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo HTTP_CSS_PATH; ?>fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>atlantis.css">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>demo.css">
	
	<script src="<?php echo HTTP_JS_PATH; ?>core/jquery.3.2.1.min.js"></script>
	<style>
	.swal-text {
  background-color: #FEFAE3;
  padding: 17px;
  border: 1px solid #F0E1A1;
  display: block;
  margin: 22px;
  text-align: center;
  color: #61534e;
  cursor:pointer;
}
	</style>
</head>
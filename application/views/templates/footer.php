
	<script src="<?php echo HTTP_JS_PATH; ?>core/popper.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH; ?>core/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo HTTP_JS_PATH; ?>plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH; ?>plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<!-- Bootstrap Toggle -->
	<script src="<?php echo HTTP_JS_PATH; ?>plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
	<!-- jQuery Scrollbar -->
	<script src="<?php echo HTTP_JS_PATH; ?>plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<!-- Atlantis JS -->
	<script src="<?php echo HTTP_JS_PATH; ?>atlantis.min.js"></script>
	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="<?php echo HTTP_JS_PATH; ?>setting-demo2.js"></script>
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/forms/forms.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 04 Oct 2019 20:31:24 GMT -->
</html>
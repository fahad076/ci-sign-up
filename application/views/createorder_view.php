<?php $this->load->view('templates/header');?>

<body>
	<div class="wrapper sidebar_minimize">
		<?php $this->load->view('templates/nav');?>
		
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title"><?php echo $pageName; ?></div>
								</div>
								<div class="card-body">
									<div class="row">
									<div class="col-md-2 col-lg-2"></div>
									
										<div class="col-md-8 col-lg-8">
											<form>
												<div class="form-group">
													<label for="agentName">Agent Name</label>
													<select class="form-control" id="agentName" name="agentName">
														<option value="" selected>Select Agent</option>
														<option value="Mark Cady">Mark Cady</option>
														<option value="Joe Fernandez">Joe Fernandez</option>
													</select>
												</div>
												<!--
												<div class="form-group">
													<label for="orderReference">Order Reference</label>
													<input type="text" class="form-control" id="orderReference" name="orderReference" placeholder="Order Reference" value="<?php echo $refNo; ?>" readonly>
												</div>
												-->
												<div class="form-group">
													<label for="clientName">Client Name</label>
													<input type="text" class="form-control" id="clientName" name="clientName" placeholder="Order Name">
												</div>
												<div class="form-group">
													<label for="packageDetails">Package Details</label>
													<input type="text" class="form-control" id="packageDetails" name="packageDetails" placeholder="Package Details">
												</div>
												<div class="form-group">
													<label for="currency">Currency</label>
													<select class="form-control" id="currency" name="currency">
														<option value="USD" selected>USD</option>
														<option value="GBP">GBP</option>
														<option value="AUD">AUD</option>
														<option value="CAD">CAD</option>
													</select>
												</div>
												<div class="form-group">
													<label for="amount">Amount</label>
													<input min="1" type="number" class="form-control" id="amount" name="amount" placeholder="Amount">
												</div>
												<div class="card-action">
													
													<button type="button" class="btnSubmit btn btn-success">Submit</button>
													
												</div>
											</form>											
										</div>
										
										
										<div class="col-md-2 col-lg-2"></div>
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						
					</nav>
					<div class="copyright ml-auto">
						<?php echo date('Y'); ?>, made with <i class="fa fa-heart heart text-danger"></i> by <a href="#">ThemeKita</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
	
	
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
	$('.document').ready(function() {
		
	
		
		$('.btnSubmit').click(function() {
				var t = this;
				
				$(t).html('Processing...').prop('disabled',true);
				$.ajax({
					url:'<?php echo base_url().'req-create-order';?>',
					type:'POST',
					data: $(t).closest('form').serializeArray()
				}).done(function(data){
					$(t).html('Submit').prop('disabled',false);
					$(t).next('div').remove();
					var dataa = JSON.parse(data);
					if(dataa.status == '1'){
					swal(
					"Payment Link",  //title
					dataa.payLink,  //text
					"success",  //icon
					{
					closeOnClickOutside: false, // prevent close on click anywhere/outside

					}
					).then(ok => {
					if (ok) {
						location.reload();
					}
					});
							
						//window.location.href=dataa.payLink;
					}else{
						$("<div style='color:red; float:right;'>"+dataa+"</div>").insertAfter(t);
					}
				});
			});
        });
	</script>
	<?php $this->load->view('templates/footer');?>
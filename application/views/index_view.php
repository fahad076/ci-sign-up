<?php $this->load->view('templates/header');?>
<body>
	<div class="wrapper sidebar_minimize">
		<?php $this->load->view('templates/nav');?>		
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">					
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title"><?php echo $pageName; ?></div>
								</div>
								<div class="card-body">
									<div class="row">
									<div class="col-md-2 col-lg-2"></div>									
										<div class="col-md-8 col-lg-8">
											<form id="UserForm" method="POST" action="<?php echo base_url().'req-submit-user'; ?>" enctype="multipart/form-data">
												<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
												<div class="form-group">
													<label for="firstName">First Name</label>
													<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
												</div>
												<div class="form-group">
													<label for="lastName">Last Name</label>
													<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
												</div>
												<div class="form-group">
													<label for="dob">Date Of Birth</label>
													<input type="date" class="form-control" id="dob" name="dob" placeholder="Date Of Birth">
												</div>
												<div class="form-group">
													<label for="photoUpload">Upload Photo</label>
													<input type="file" class="form-control" id="photoUpload" name="file">
												</div>
												<div class="card-action">													
													<button type="button" class="btnSubmit btn btn-success">Submit</button>													
												</div>
											</form>											
										</div>
										<div class="col-md-2 col-lg-2"></div>
									</div>									
								</div>								
							</div>
						</div>
					</div>					
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">						
					</nav>
					<div class="copyright ml-auto">
						<?php echo date('Y'); ?>, made with <i class="fa fa-heart heart text-danger"></i> by <a href="#">ThemeKita</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
	
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
	$('.document').ready(function() {	
		$('.btnSubmit').click(function() {
			var t = this;			
			$(t).html('Processing...').prop('disabled',true);
			$.ajax({
				url:'<?php echo base_url().'req-create-user';?>',
				type:'POST',
				data: $(t).closest('form').serializeArray()
			}).done(function(data){
				$(t).html('Submit').prop('disabled',false);
				$(t).next('div').remove();
				var dataa = JSON.parse(data);
				if(dataa == '1')
				{
					//window.location.href=dataa.payLink;
					$("#UserForm").submit();
				}
				else
				{
					$("<div style='color:red; float:right;'>"+dataa+"</div>").insertAfter(t);
				}
			});
		});
    });
	</script>
	<?php $this->load->view('templates/footer');?>
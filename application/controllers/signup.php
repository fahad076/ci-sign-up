<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }

    public function index()
    {
        $data['pageName'] = 'Sign Up';
        $this->load->view('index_view', $data);
    }
	
	public function CreateUser(){
		
		$config = array(
            array(
                'field' => 'firstName',
                'label' => 'First Name',
                'rules' => 'trim|required|max_length[255]|xss_clean',
            ),
			array(
                'field' => 'lastName',
                'label' => 'Last Name',
                'rules' => 'trim|required|max_length[255]|xss_clean',
            ),
			array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim|required|max_length[15]|xss_clean',
            )
        );
        $this->form_validation->set_message('required', 'Enter %s');
        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            if (form_error('firstName')) 
                echo json_encode(form_error('firstName'));
			elseif (form_error('lastName'))
                echo json_encode(form_error('lastName'));
            elseif (form_error('dob'))
				echo json_encode(form_error('dob'));
        }
        else
            echo 1;
	}

    private function UploadImageConf() 
    {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2000;
        $config['max_width'] = 1500;
        $config['max_height'] = 1500;

        $this->load->library('upload', $config);
    }

    public function SubmitUser()
    {
        $this->UploadImageConf();
        if (!$this->upload->do_upload('file')){
            $data['error'] = $this->upload->display_errors();
            $this->load->view('message_view', $data);
        }
        else
        {
            $image = $this->upload->data();
            $data = array(
                "FirstName" => $this->input->post('firstName'),
                "LastName" => $this->input->post('lastName'),
                "DOB" => $this->input->post('dob'),
                "Photo" => !empty($image) ? $image['file_name'] : "",
                "CreateDate" => gmdate("Y-m-d H:i:s")
            );
    
            $this->signup_model->InsertDB('users',$data);
            $data['success'] = "User sign up successfully.";
            $this->load->view('message_view', $data);
        }        
    }
}